// This program is free software licenced under MIT Licence. You can
// find a copy of this licence in LICENCE.txt in the top directory of
// source code.
//
// Copyright (C) 2016 AproapeBine (Calin Cruceru, Mihai Dumitru, Radu Oancea)
//

// Self
#include "exercise.hpp"
#include "db_manager.hpp"

// C++
#include <utility>
#include <cassert>

namespace tonedear {

exercise::exercise(QString name, QString description, QString seq, QObject *parent)
    : QObject(parent) {

    QSqlQuery query1{};

    query1.prepare("INSERT INTO Exercises (name, description, seq, log) "
                  "VALUES (:name, :description, :seq, :log)");
    query1.bindValue(":name", name);
    query1.bindValue(":description", description);
    query1.bindValue(":seq", seq);
    query1.bindValue(":log", QByteArray());

    run_query(query1, "Adding exercise failed");
    m_id = query1.lastInsertId().toInt();
    qDebug() << m_id;
}

exercise::exercise(int id, QObject *parent)
    : QObject(parent), m_id(id) {
}

exercise::exercise(QObject *parent)
    : QObject(parent) {
}

void exercise::del() {
    QSqlQuery query{};

    query.prepare("DELETE FROM Exercises WHERE id = ?");
    query.addBindValue(m_id);
    run_query(query, "Delete exercise failed");
}

int exercise::id() const { return m_id; }

QString exercise::name() const {
    QSqlQuery query{};

    query.prepare("SELECT name FROM Exercises WHERE id = ?");
    query.addBindValue(m_id);
    run_query(query, "SELECT exercise name failed");
    assert(query.next());
    return query.value(0).toString();
}

void exercise::set_name(const QString &name) {
    QSqlQuery query{};

    query.prepare("UPDATE Exercises SET name = ? WHERE id = ?");
    query.addBindValue(name);
    query.addBindValue(m_id);
    run_query(query, "UPDATE exercise name failed");
    Q_EMIT name_changed();
}

QString exercise::description() const {
    QSqlQuery query{};

    query.prepare("SELECT description FROM Exercises WHERE id = ?");
    query.addBindValue(m_id);
    run_query(query, "SELECT exercise description failed");
    assert(query.next());
    return query.value(0).toString();
}

void exercise::set_description(const QString &description) {
    QSqlQuery query{};

    query.prepare("UPDATE Exercises SET description = ? WHERE id = ?");
    query.addBindValue(description);
    query.addBindValue(m_id);
    run_query(query, "UPDATE exercise description failed");
    Q_EMIT description_changed();
}

QString exercise::seq() const {
    QSqlQuery query{};

    query.prepare("SELECT seq FROM Exercises WHERE id = ?");
    query.addBindValue(m_id);
    run_query(query, "SELECT exercise seq failed");
    assert(query.next());
    return query.value(0).toString();
}

void exercise::set_seq(const QString &seq) {
    QSqlQuery query{};

    query.prepare("UPDATE Exercises SET seq = ? WHERE id = ?");
    query.addBindValue(seq);
    query.addBindValue(m_id);
    run_query(query, "UPDATE exercise seq failed");
    Q_EMIT seq_changed();
}

void exercise::reset_log() {
    QSqlQuery query{};
    query.prepare("UPDATE Exercises SET log = ? WHERE id = ?");
    query.addBindValue(QByteArray());
    query.addBindValue(m_id);
    run_query(query, "UPDATE reset exercise log failed");
    Q_EMIT log_changed();
}

void exercise::add_to_log(bool correct) {
    QByteArray log = get_log_blob();
    log.append(correct);

    QSqlQuery query{};
    query.prepare("UPDATE Exercises SET log = ? WHERE id = ?");
    query.addBindValue(log);
    query.addBindValue(m_id);
    run_query(query, "UPDATE exercise log failed");
    Q_EMIT log_changed();
}

QByteArray exercise::get_log_blob() const {
    QSqlQuery query{};

    query.prepare("SELECT log FROM Exercises WHERE id = ?");
    query.addBindValue(m_id);
    run_query(query, "SELECT exercise log failed");
    assert(query.next());
    return query.value(0).toByteArray();
}

QList<bool> exercise::get_log() const {
    QByteArray log = get_log_blob();

    auto ret = QList<bool>{};
    for (auto i = 0; i < log.size(); i++)
        ret.append(log[i]);
    return ret;
}

} // namespace tonedear
