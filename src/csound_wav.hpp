// This program is free software licenced under MIT Licence. You can
// find a copy of this licence in LICENCE.txt in the top directory of
// source code.
//
// Copyright (C) 2016 AproapeBine (Calin Cruceru, Mihai Dumitru, Radu Oancea)
//

#ifndef _CSOUND_WAV_HPP
#define _CSOUND_WAV_HPP 1

// Self
#include "sequence_generator/sequence_generator.hpp"

// C++
#include <string>

namespace tonedear {

/**
 * Generates a .wav containing the audio representation of @seq, using Csound.
 *
 * The .wav file is placed in the current working directory.
 *
 * File format: csound default
 *
 * Returns 0 in case of success.
 */
int generate_wav_file(const generated_seq_ptr seq,
                      const std::string& filename = "test.wav");

} // namespace tonedear

#endif // _CSOUND_WAV_HPP

