// This program is free software licenced under MIT Licence. You can
// find a copy of this licence in LICENCE.txt in the top directory of
// source code.
//
// Copyright (C) 2016 AproapeBine (Calin Cruceru, Mihai Dumitru, Radu Oancea)
//

// Self
#include "tonedear_logic.hpp"
#include "csound_wav.hpp"

// Qt
#include <QDebug>
#include <QtAlgorithms>
#include <QStandardPaths>
#include <QDir>
#include <QVariant>
#include <QSound>

// C++
#include <algorithm>
#include <cassert>

namespace tonedear {
namespace {
    // STATIC DATA
    static const auto s_soundfile
        = QStandardPaths::writableLocation(QStandardPaths::TempLocation)
           + QDir::separator() + "test.wav";

} // namespace {

tonedear_logic::tonedear_logic(db_manager &db_mgr, QObject *parent)
    : QObject{parent}
    , m_db_mgr{db_mgr}
    , m_exercises{m_db_mgr.get_exercises()} {
}

tonedear_logic::~tonedear_logic() {
    qDeleteAll(m_exercises);
}

QQmlListProperty<tonedear::exercise> tonedear_logic::exercises() {
    return QQmlListProperty<tonedear::exercise>(this, m_exercises);
}

void tonedear_logic::add_exercise(const QString &name,
                                  const QString &description,
                                  const QString &seq) {
    auto ex = new exercise{name, description, seq};

    m_exercises.append(ex);
    Q_EMIT exercises_changed();
}

void tonedear_logic::remove_exercise(exercise *ex)
{
    for (auto it = m_exercises.begin(); it != m_exercises.end(); ++it) {
        if (*it == ex) {
            m_exercises.erase(it);
            ex->del();
            Q_EMIT exercises_changed();
            delete ex;

            return;
        }
    }

    assert(0);
}

void tonedear_logic::play_sequence() const {
    assert(generate_wav_file(m_seq, s_soundfile.toStdString()) == 0);
    QSound::play(s_soundfile);
}

QString tonedear_logic::get_sequence() const {
    assert(m_seq != nullptr);
    return QString::fromStdString(sequence_to_string(m_seq));
}

void tonedear_logic::generate_sequence(const QString &seq_string) {
    auto parsed_seq = parse_musical_seq(seq_string.toStdString());

    assert(parsed_seq != nullptr);
    m_seq = tonedear::generate_sequence(parsed_seq);
}

bool tonedear_logic::validate_sequence(const QString &seq_string) {
    auto parsed_seq = parse_musical_seq(seq_string.toStdString());

    return (parsed_seq != nullptr);
}

} // namespace tonedear

