// This program is free software licenced under MIT Licence. You can
// find a copy of this licence in LICENCE.txt in the top directory of
// source code.
//
// Copyright (C) 2016 AproapeBine (Calin Cruceru, Mihai Dumitru, Radu Oancea)
//

#include "utils.h"

#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

void (*error_handler)(const char *format, va_list args) = stderr_handler;

int error(int err_res, const char *format, ...)
{
	va_list args;

	va_start(args, format);

	if (error_handler)
		error_handler(format, args);

	va_end(args);
	return err_res;
}

void stderr_handler(const char *format, va_list args)
{
	vfprintf(stderr, format, args);
}

