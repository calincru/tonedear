// This program is free software licenced under MIT Licence. You can
// find a copy of this licence in LICENCE.txt in the top directory of
// source code.
//
// Copyright (C) 2016 AproapeBine (Calin Cruceru, Mihai Dumitru, Radu Oancea)
//

#ifndef UTILS_H_
#define UTILS_H_ 1

#ifdef __cplusplus
extern "C" {
#endif

// C
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>

#define UNUSED(a) ((void)(&a))

/* error printing macro */
#ifdef DEBUG
# define ERR(call_description)						\
	do {								\
		fprintf(stderr, "[%s(), %s:%u]: ",			\
			__FUNCTION__, __FILE__, __LINE__);		\
		perror(call_description);				\
	} while (0)
#else
# define ERR(call_description) perror(call_description)
#endif


#define DIE(assertion, call_description)				\
	do {								\
		if (assertion) {					\
			ERR(call_description);				\
			_exit(EXIT_FAILURE);				\
		}							\
	} while (0)


#ifdef DEBUG

# define DPRINTF(format, ...)						\
	((void)fprintf(stderr, "[%s(), %s:%d] " format,			\
		__FUNCTION__, __FILE__, __LINE__,			\
		##__VA_ARGS__))
#else
# define DPRINTF(msg, ...)						\
		do {} while (0)		/* do nothing */
#endif /* defined(DEBUG) */


#if defined DEBUG

/* log levels */
enum {
	LOG_EMERG = 1,
	LOG_ALERT,
	LOG_CRIT,
	LOG_ERR,
	LOG_WARNING,
	LOG_NOTICE,
	LOG_INFO,
	LOG_DEBUG
};

/*
 * initialize default loglevel (for dlog)
 * may be redefined in the including code
 */
# ifndef LOG_LEVEL
#  define LOG_LEVEL	LOG_WARNING
# endif

# define DLOG(level, format, ...)					\
		do {							\
			if (level <= LOG_LEVEL)				\
				DPRINTF(format, ##__VA_ARGS__);		\
		} while (0)
#else
# define DLOG(level, format, ...)					\
		do {} while (0)		/* do nothing */
#endif /* defined DEBUG */


/**
 * Returns the given error code and pases @format (and the printf-like variadic
 * list following it) to the error_handler function, if it is not NULL.
 * Set error_handler to NULL, for no action on the string.
 */
extern int error(int err_res, const char *format, ...);

/**
 * This function is used to handle the format and variadic list passed by
 * functions like error
 */
extern void (*error_handler)(const char *format, va_list args);

/**
 * Prints @format to stderr using the variadic list @args
 */
extern void stderr_handler(const char *format, va_list args);

#ifdef __cplusplus
}
#endif

#endif /* UTILS_H_ */

