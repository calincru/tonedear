// This program is free software licenced under MIT Licence. You can
// find a copy of this licence in LICENCE.txt in the top directory of
// source code.
//
// Copyright (C) 2016 AproapeBine (Calin Cruceru, Mihai Dumitru, Radu Oancea)
//

#ifndef _SEQUENCE_GENERATOR_HPP
#define _SEQUENCE_GENERATOR_HPP 1

// Project
#include "../parser/parser.hpp"

// C++
#include <vector>
#include <memory>
#include <string>

namespace tonedear {

struct generated_seq_t {
    std::vector<note_ptr> notes;
};

using generated_seq_ptr = std::shared_ptr<generated_seq_t>;

/**
 * Returns a sequence of notes from the specification of a musical sequence.
 * More specifically, it copies all notes and makes a random selection from each
 * choice sequence.
 * @seq should be obtained from the parser. If any seubsequence has no elements,
 * a division by 0 will occur (the parser should not produce such a result).
 *
 * Example:
 * "A4(G#4|F#4)C4C4(F4|F#4|G#4)" -> can result in 6 (= 2 * 3) possible
 * sequences, such as:
 * "A4F#4C4C4F4"
 */
generated_seq_ptr generate_sequence(const musical_seq_ptr seq);

/**
 * Converts a @seq to a string.
 * The letters are written using letter notation (capital letters) and the sharp
 * ('#') symbol, followed by a digit (0-8) representing the octave. Notes are
 * separated by a single space.
 *
 * Example:
 * "E5 D#5 E5 D#5 E5 B4 D5 C5 A4"
 */
std::string sequence_to_string(const generated_seq_ptr seq);

/**
 * Generates a string representing a csound orchestra file, based on a musical
 * sequence.
 * @seq should be obtained from the parser.
 *
 * More information about the format:
 * https://csound.github.io/docs/manual/OrchTop.html
 */
std::string sequence_to_csound_orchestra(const generated_seq_ptr seq);

/**
 * Generates a string representing a csound score file, based on a musical
 * sequence.
 * @seq should be obtained from the parser.
 *
 * More information about the format:
 * http://www.csounds.com/tootsother/vercoetut/Vercoe.html
 */
std::string sequence_to_csound_score(const generated_seq_ptr seq);

} /* namespace tonedear */

#endif /* _SEQUENCE_GENERATOR_HPP */

