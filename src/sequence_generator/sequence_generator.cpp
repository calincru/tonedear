// This program is free software licenced under MIT Licence. You can
// find a copy of this licence in LICENCE.txt in the top directory of
// source code.
//
// Copyright (C) 2016 AproapeBine (Calin Cruceru, Mihai Dumitru, Radu Oancea)
//

// Self
#include "sequence_generator.hpp"

// Project
#include "../utils/utils.h"

// C++
#include <cstdlib>
#include <ctime>
#include <vector>
#include <string>
#include <sstream>
#include <iomanip>
#include <iostream>

namespace tonedear {

static void copy_subseq_notes(std::vector<note_ptr>& v,
                              const musical_subseq_ptr ss)
{
    for (auto note : ss->notes)
        v.push_back(note);
}

static void fill_notes(std::vector<note_ptr>& v, const musical_seq_ptr seq)
{
    switch (seq->type) {
        case CHOICE: {
            DLOG(LOG_DEBUG, "Expanding choice\n");
            auto index = std::rand() % seq->choice->subseqs.size();
            copy_subseq_notes(v, seq->choice->subseqs[index]);
            break;
        }

        case SEQSEQ:
            DLOG(LOG_DEBUG, "Expanding seqseq\n");
            fill_notes(v, seq->seq1);
            fill_notes(v, seq->seq2);
            break;

        case SUBSEQ:
            DLOG(LOG_DEBUG, "Expanding subseq\n");
            copy_subseq_notes(v, seq->subseq);
            break;
    }
}

generated_seq_ptr generate_sequence(const musical_seq_ptr seq)
{
    generated_seq_ptr result(new generated_seq_t());

    std::srand(std::time(nullptr));
    fill_notes(result->notes, seq);
    return result;
}

std::string note_to_string(note_ptr note)
{
    return note_names[note->semitone] + (char) (note->octave + '0');
}

std::string sequence_to_string(const generated_seq_ptr seq)
{
    std::string result;

    for (auto& note : seq->notes)
        result += note_to_string(note) + ' ';

    return result;
}

std::string sequence_to_csound_orchestra(const generated_seq_ptr seq)
{
    static const std::string orchestra =
        "sr = 44100 ; sample rate\n"
        "ksmps = 10 ; control signal rate\n"
        "nchnls = 1 ; number of output channels\n"
        "\n"
        "instr 1\n"
        "\ticps\tinit\tcpspch(p5) ; Get target pitch from score event\n"
        "\tidur\tinit\tp3 ; Get target pitch from score event\n"
        "\tiamp0\tinit\tp4 ; Set default amps\n"
        "\n"
        "\tkenv\tlinen\tiamp0, .05, idur, .05\n"
        "\tar\toscil\tkenv, icps, 1\n"
        "\tout ar\n"
        "endin\n"
        ;

    UNUSED(seq); /* the orchestra doesn't depend on the sequence */
    return orchestra;
}

std::string sequence_to_csound_score(const generated_seq_ptr seq)
{
    /* CHANGE THESE TO CONTROL THE SCORE FILE */
    static constexpr unsigned table_number = 1;
    static constexpr unsigned table_creation_time = 0;
    static constexpr unsigned table_size = 8192;
    static constexpr unsigned table_soubroutine = 10;
    static constexpr unsigned table_soubroutine_args[] = { 1 };

    static constexpr unsigned instrument_number = 1;

    static constexpr float note_duration = 1.0f; /* in seconds */
    static constexpr unsigned note_amplitude = 20000;

    /* passed to manipulators */
    static constexpr int field_width = 8;
    static constexpr int precision = 2;
    /* END */

    std::ostringstream ss;
    ss.precision(precision);
    ss << std::fixed;

    /* table entry that defines the instrument */
    ss << 'f' << table_number;

    ss << std::setw(field_width) << table_creation_time
       << std::setw(field_width) << table_size
       << std::setw(field_width) << table_soubroutine;
    for (const auto& arg : table_soubroutine_args)
        ss << std::setw(field_width) << arg;

    ss << '\n';

    float start_time = 0.0f;
    float stop_time = note_duration;
    for (const auto& note : seq->notes) {
        ss << 'i' << instrument_number;
        ss << std::setw(field_width) << start_time
           << std::setw(field_width) << note_duration
           << std::setw(field_width) << note_amplitude
           << std::setw(field_width) << (unsigned) note->octave + 4;
        ss << '.' << std::setfill('0') << std::setw(2)
           << (unsigned) note->semitone
           << std::setfill(' ') << '\n';

        start_time += note_duration;
        stop_time += note_duration;
    }
    ss << "e\n";

    return ss.str();
}

} /* namespace tonedear */

