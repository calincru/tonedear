// find a copy of this licence in LICENCE.txt in the top directory of
// source code.
//
// Copyright (C) 2016 AproapeBine (Calin Cruceru, Mihai Dumitru, Radu Oancea)
//

// Project
#include "parser/parser.hpp"
#include "sequence_generator/sequence_generator.hpp"

// Csound
#include <csound/csound.h>

// C++
#include <string>

namespace tonedear {

/** Handle for csound messages to keep it quiet */
static void csound_no_messages(CSOUND *, int, const char *, va_list) {}

int generate_wav_file(const generated_seq_ptr seq,
                      const std::string& filename = "test.wav")
{
    // no output to stderr
    csoundSetDefaultMessageCallback(csound_no_messages);

    auto csound = csoundCreate(0);
    auto output_arg = "-o" + filename;
    int ret;

    ret = csoundSetOption(csound, (char *) "-d"); // no display
    if (ret != CSOUND_SUCCESS)
        goto _exit;

    ret = csoundSetOption(csound, (char *) output_arg.c_str()); // output name
    if (ret != CSOUND_SUCCESS)
        goto _exit;

    // no documentation on what these return?!
    csoundCompileOrc(csound, sequence_to_csound_orchestra(seq).c_str());
    csoundReadScore(csound, sequence_to_csound_score(seq).c_str());
    csoundStart(csound);

    ret = csoundPerform(csound);
    if (ret >= 0)
        ret = 0;

_exit:
    csoundDestroy(csound);
    return ret;
}

} // namespace tonedear

