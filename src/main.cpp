// This program is free software licenced under MIT Licence. You can
// find a copy of this licence in LICENCE.txt in the top directory of
// source code.
//
// Copyright (C) 2016 AproapeBine (Calin Cruceru, Mihai Dumitru, Radu Oancea)
//

// Project
#include "db_manager.hpp"
#include "tonedear_logic.hpp"

// Qt
#include <QApplication>
#include <QQmlApplicationEngine>
#include <QStandardPaths>
#include <QDir>
#include <QtQml>

// C++
#include <cstdlib>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QString appDataPath
        = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);

    if (appDataPath == "") {
        qDebug() << "Error: Couldn't find application data path";
        exit(EXIT_FAILURE);
    }
    if (!QDir(appDataPath).exists()) {
        QDir().mkdir(appDataPath);
    }

    tonedear::db_manager manager{appDataPath + QDir::separator() + "db.sqlite"};
    tonedear::tonedear_logic logic{manager};

    qmlRegisterType<tonedear::exercise>("ToneDear", 1, 0, "Exercise");
    QQmlApplicationEngine engine(QUrl("qrc:/Application.qml"));
    engine.rootContext()->setContextProperty("ToneDear", &logic);

    return app.exec();
}

