// This program is free software licenced under MIT Licence. You can
// find a copy of this licence in LICENCE.txt in the top directory of
// source code.
//
// Copyright (C) 2016 AproapeBine (Calin Cruceru, Mihai Dumitru, Radu Oancea)
//

#ifndef _EXERCISE_HPP
#define _EXERCISE_HPP 1

// Qt
#include <QObject>
#include <QString>
#include <QDebug>

// C++
#include <string>

namespace tonedear {

class exercise : public QObject {

    Q_OBJECT
    Q_PROPERTY(int id READ id)
    Q_PROPERTY(QString name
               READ name
               WRITE set_name
               NOTIFY name_changed)
    Q_PROPERTY(QString description
               READ description
               WRITE set_description
               NOTIFY description_changed)
    Q_PROPERTY(QString seq
               READ seq
               WRITE set_seq
               NOTIFY seq_changed)
    Q_PROPERTY(QList<bool> log
               READ get_log
               NOTIFY log_changed)

public:
    exercise(QString name, QString description, QString seq, QObject *parent = 0);
    exercise(int id, QObject *parent = 0);
    exercise(QObject *parent = 0);
    ~exercise() = default;

    void del();

    int id() const;

    QString name() const;
    void set_name(const QString &name);

    QString description() const;
    void set_description(const QString &description);

    QString seq() const;
    void set_seq(const QString &seq);

    Q_INVOKABLE void reset_log();
    Q_INVOKABLE void add_to_log(bool correct);
    QByteArray get_log_blob() const;
    QList<bool> get_log() const;

Q_SIGNALS:
    void name_changed();
    void description_changed();
    void seq_changed();
    void log_changed();

private:
    int m_id;

}; // class exercise

} // namespace tonedear
#endif // _EXERCISE_HPP

