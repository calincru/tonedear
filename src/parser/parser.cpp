// This program is free software licenced under MIT Licence. You can
// find a copy of this licence in LICENCE.txt in the top directory of
// source code.
//
// Copyright (C) 2016 AproapeBine (Calin Cruceru, Mihai Dumitru, Radu Oancea)
//

// Self
#include "parser.hpp"

// Project
#include "../utils/utils.h"

// C++
#include <cctype>
#include <string>
#include <vector>
#include <algorithm>

namespace tonedear {

/* useful aliases */
#define ARRAYSIZE(arr) (sizeof(arr) / sizeof(0[arr]))
#define MIN_OCTAVE_DIGIT '0'
#define MAX_OCTAVE_DIGIT '8'

using size_type = std::string::size_type;

static uint8_t get_note_number(const std::string& expr_str, size_type start)
{
    uint8_t i;

    uint8_t size = ARRAYSIZE(note_names);
    for (i = size; i > 0; --i) // in reverse to catch sharps
        if (!expr_str.compare(start, note_names[i-1].length(), note_names[i-1]))
            break;

    return i-1;
}

static note_ptr parse_note(const std::string& expr_str)
{
    note_ptr note(new note_t());

    DLOG(LOG_INFO, "Parsing string: \"%s\"\n", expr_str.c_str());

    note->semitone = get_note_number(expr_str, 0);
    if (note->semitone >= ARRAYSIZE(note_names)) {
        DLOG(LOG_NOTICE, "Invalid note: %" PRIu8 "\n", note->semitone);
        return nullptr;
    }
    DLOG(LOG_DEBUG, "Found semitone: %" PRIu8 "\n", note->semitone);

    auto i = note_names[note->semitone].length();
    if (expr_str[i] < MIN_OCTAVE_DIGIT || expr_str[i] > MAX_OCTAVE_DIGIT){
        DLOG(LOG_NOTICE, "Invalid octave number: %c\n", expr_str[i]);
        return nullptr;
    }

    note->octave = expr_str[i] - '0';
    DLOG(LOG_DEBUG, "Found octave: %" PRIu8 "\n", note->octave);

    return note;
}

static size_type get_first_non_space_index(const std::string& expr_str,
                                           size_type index)
{
    for (size_type i = index; i < expr_str.length(); ++i)
        if (!std::isspace(expr_str[i]))
            return i;

    return std::string::npos;
}

static bool last_nonspace_at(const std::string& expr_str, size_type index)
{
    for (auto i = expr_str.length() - 1; i > index; --i)
        if (!std::isspace(expr_str[i]))
            return false;

    return true;
}

static musical_subseq_ptr parse_musical_subseq(const std::string& expr_str)
{
    musical_subseq_ptr result(new musical_subseq_t());
    size_type i = 0;

    DLOG(LOG_INFO, "Parsing string: \"%s\"\n", expr_str.c_str());

    while (true) {
        i = get_first_non_space_index(expr_str, i);
        if (i == std::string::npos)
            break;

        auto note = parse_note(expr_str.substr(i));
        if (!note) {
            DLOG(LOG_INFO, "Invalid note\n");
            return nullptr;
        }

        result->notes.push_back(note);
        i += note_names[note->semitone].length() + 1;
        DLOG(LOG_DEBUG, "i = %zu\n", (size_t) i);
    }

    return result;
}

static choice_seq_ptr parse_choice(const std::string& expr_str)
{
    choice_seq_ptr choice(new choice_seq_t());
    size_type last_pos = 0;

    DLOG(LOG_INFO, "Parsing string: \"%s\"\n", expr_str.c_str());

    size_type pos;
    do {
        pos = expr_str.find('|', last_pos);

        musical_subseq_ptr subseq;
        if (pos == std::string::npos)
            subseq = parse_musical_subseq(expr_str.substr(last_pos));
        else
            subseq = parse_musical_subseq(expr_str.substr(last_pos, pos - last_pos));

        if (!subseq)
            return nullptr;

        choice->subseqs.push_back(subseq);

        last_pos = pos + 1;
    } while (pos != std::string::npos);

    return choice;
}

musical_seq_ptr parse_musical_seq(const std::string& arg_str)
{
    musical_seq_ptr result(new musical_seq_t());

    DLOG(LOG_INFO, "Parsing string: \"%s\"\n", expr_str.c_str());

    auto expr_str = arg_str;
    std::transform(expr_str.begin(), expr_str.end(), expr_str.begin(),
                   [](char c) { return toupper(c); });

    auto i = get_first_non_space_index(expr_str, 0);
    if (i == std::string::npos)
        return nullptr;

    if (expr_str[i] == '(') { /* choice */
        auto choice_end = expr_str.find(')');
        if (choice_end == std::string::npos) {
            DLOG(LOG_NOTICE, "Unterminated choice sequence\n");
            return nullptr;
        }

        if (last_nonspace_at(expr_str, choice_end)) {/* whole seq is a choice */
            DLOG(LOG_DEBUG, "Entire sequence is a choice\n");
            result->type = CHOICE;
            result->choice = parse_choice(expr_str.substr(i+1, choice_end-i-1));
            if (!result->choice) {
                DLOG(LOG_NOTICE, "Invalid choice\n");
                return nullptr;
            }

            return result;
        }

        result->type = SEQSEQ;
        result->seq1 = parse_musical_seq(expr_str.substr(i, choice_end-i+1));
        if (!result->seq1) {
            DLOG(LOG_NOTICE, "Invalid first sequence\n");
            return nullptr;
        }

        result->seq2 = parse_musical_seq(expr_str.substr(choice_end+1));
        if (!result->seq2) {
            DLOG(LOG_NOTICE, "Invalid second sequence\n");
            return nullptr;
        }

        return result;
    }

    auto choice_begin = expr_str.find('(');
    if (choice_begin == std::string::npos) { /* whole seq is a subseq */
        DLOG(LOG_DEBUG, "Entire sequence is a subsequence\n");
        result->type = SUBSEQ;
        result->subseq = parse_musical_subseq(expr_str.substr(i));
        if (!result->subseq) {
            DLOG(LOG_NOTICE, "Invalid subsequence\n");
            return nullptr;
        }

        return result;
    }

    result->type = SEQSEQ;
    result->seq1 = parse_musical_seq(expr_str.substr(i, choice_begin-i));
    if (!result->seq1) {
        DLOG(LOG_NOTICE, "Invalid subsequence\n");
        return nullptr;
    }

    result->seq2 = parse_musical_seq(expr_str.substr(choice_begin));
    if (!result->seq2) {
        DLOG(LOG_NOTICE, "Invalid subsequence\n");
        return nullptr;
    }

    return result;
}

} /* namespace tonedear */

