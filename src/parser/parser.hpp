// This program is free software licenced under MIT Licence. You can
// find a copy of this licence in LICENCE.txt in the top directory of
// source code.
//
// Copyright (C) 2016 AproapeBine (Calin Cruceru, Mihai Dumitru, Radu Oancea)
//

#ifndef _PARSER_HPP
#define _PARSER_HPP 1

// C++
#include <cinttypes>
#include <vector>
#include <string>
#include <memory>

namespace tonedear {

static const std::string note_names[] = {
    "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"
};

/** Forward definitions */
struct note_t;
struct choice_seq_t;
struct musical_subseq_t;
struct musical_seq_t;

using note_ptr = std::shared_ptr<note_t>;
using choice_seq_ptr = std::shared_ptr<choice_seq_t>;
using musical_subseq_ptr = std::shared_ptr<musical_subseq_t>;
using musical_seq_ptr = std::shared_ptr<musical_seq_t>;

/**
 * Models a single musical note.
 * @octave number of the octave (ranging from 0 to 8)
 * @semitones number of semitones from "c" (c is 0, c# is 1, d is 2 etc.)
 */
struct note_t {
    uint8_t octave;
    uint8_t semitone;
};

/**
 * Models a choice between multiple musical subsequences.
 * @subseqs a list of pointers to each of the subsequences.
 */
struct choice_seq_t {
    std::vector<musical_subseq_ptr> subseqs;
};

/**
 * Models a musical subsequence.
 * A musical subsequence is a list of notes.
 */
struct musical_subseq_t {
    std::vector<note_ptr> notes;
};

/**
 * Models a musical sequence.
 *
 * A musical sequence is either a choice between multiple sequences, two 
 * consecutive sequences, or a simple note.
 * Whichever the type, the rest of the pointers are null.
 * @type stores the type of the sequence
 */
enum musical_seq_type_t { CHOICE, SEQSEQ, SUBSEQ };

struct musical_seq_t {
    enum musical_seq_type_t type;
    choice_seq_ptr choice;
    musical_subseq_ptr subseq;
    musical_seq_ptr seq1;
    musical_seq_ptr seq2;
};

/**
 * Takes a string conforming to the following grammar:
 *
 * S = (T|T|...|T) | SS | T
 * T = NT | e
 * N = A0 | A#0 | ... | C8 (names of musical notes)
 *
 * The "|" operator has the lowest priority, such that "(AB|C)" should be read
 * as either "AB" or "C".
 *
 * Parses the string to create a musical_seq_t structure, to which it returns a 
 * pointer. The memory should then be released by passing the pointer to 
 * "destroy_musical_seq".
 *
 * Example of sequence string:
 * "(C#4|F4|G#4)(A#4B4|B4A#4)"
 */
musical_seq_ptr parse_musical_seq(const std::string& expr_str);

} /* namespace tonedear */

#endif /* _PARSER_HPP */

