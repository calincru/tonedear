import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2

import ToneDear 1.0
import "NavigationLogic.js" as NavigationLogic
import "."

Column {
    property Exercise exercise

    spacing: 20

    function exit() {
        navigationStack.exitExercise = NavigationLogic.exitExercise;
        navigationStack.exitExercise();
    }

    function validate() {
        return ToneDear.validate_sequence(seqField.text) &&
                nameField.text != "" &&
                descField.text != "";
    }

    function saveAndExit() {
        if (!validate()) {
            invalidDialog.open();
        } else {
            exercise.name = nameField.text;
            exercise.description = descField.text;
            exercise.seq = seqField.text;
            exit();
        }
    }

    Component.onCompleted: {
        navigationStack.exitExercise = function() {
            if (exercise.name !== nameField.text ||
                exercise.description !== descField.text ||
                exercise.seq !== seqField.text) {
                saveDialog.open();
            } else if (validate()) {
                exit();
            } else {
                invalidDialog.open();
            }
        }
    }

    MessageDialog {
        id: invalidDialog
        title: "Invalid Entry"

        icon: StandardIcon.Critical
        text: "Can't have an empty title, description or invalid sequence!"
        standardButtons: StandardButton.Ok
    }

    MessageDialog {
        id: saveDialog
        title: "Save changes"

        icon: StandardIcon.Question
        text: "Do you want to save changes?"
        standardButtons: StandardButton.Yes
                         | StandardButton.No
                         | StandardButton.Cancel

        onYes: saveAndExit()
        onNo: exit()
    }

    MessageDialog {
        id: deleteDialog
        title: "Delete exercise"

        icon: StandardIcon.Critical
        text: "Are you sure you want to delete this exercise?"
        standardButtons: StandardButton.Yes | StandardButton.No

        onYes: {
            ToneDear.remove_exercise(exercise);
            exit();
        }
    }

    Rectangle {
        anchors.left: parent.left
        anchors.leftMargin: parent.width * 0.05

        width: parent.width * 0.90
        height: parent.height / 4

        TextField {
            id: nameField
            anchors.fill: parent

            placeholderText: "Enter exercise name"
            text: exercise.name
            verticalAlignment: TextInput.AlignTop

            font {
                family: "Helvetica"
                pointSize: 16
                weight: Font.Normal
            }
        }
    }

    Rectangle {
        anchors.left: parent.left
        anchors.leftMargin: parent.width * 0.05

        width: parent.width * 0.90
        height: parent.height / 4

        TextField {
            id: descField
            anchors.fill: parent

            placeholderText: "Enter description"
            text: exercise.description
            verticalAlignment: TextInput.AlignTop

            font {
                family: "Helvetica"
                pointSize: 16
                weight: Font.Normal
            }
        }
    }

    Rectangle {
        anchors.left: parent.left
        anchors.leftMargin: parent.width * 0.05

        width: parent.width * 0.90
        height: parent.height / 4

        TextField {
            id: seqField
            anchors.fill: parent

            placeholderText: "Enter sequence specification"
            text: exercise.seq
            verticalAlignment: TextInput.AlignTop

            font {
                family: "Helvetica"
                pointSize: 16
                weight: Font.Normal
            }
        }
    }

    CustomButton {
        property color btnColor: "#0000DC"

        Layout.preferredHeight: parent.height / 10
        Layout.alignment: Qt.AlignCenter

        text: "Save & Exit"

        onClicked: saveAndExit()
    }

    CustomButton {
        property color btnColor: "#DC0000"

        Layout.preferredHeight: parent.height / 10
        Layout.alignment: Qt.AlignCenter

        text: "Delete"

        onClicked: deleteDialog.open()
    }
}
