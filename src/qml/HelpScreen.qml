import QtQuick 2.5
import QtQuick.Controls 1.4
import "Strings.js" as Strings

Rectangle {
    id:helpScreen

    Flickable {
        id: flickArea
        anchors {
            left: helpScreen.left; leftMargin: 10
        }

        anchors.fill: parent
        contentWidth: helpText.width; contentHeight: helpText.height
        flickableDirection: Flickable.VerticalFlick
        boundsBehavior: Flickable.StopAtBounds
        clip: true

        Text {
            id: helpText
            wrapMode: TextEdit.Wrap
            width:helpScreen.width - 10;

            text: Strings.help
        }
    }
}
