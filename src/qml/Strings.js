var help =
'\
<br><br>
<div align="center"><b><font size="7">Tone Dear</font></b><br></div>
<font size="4">
<p>
Tone Dear is an ear-training application, whose aim is to  develop
the ability to recognize various musical elements.
</p><p>
There are multiple types of exercises which can be edited, as well as the
possibility to create new ones.
</p><p>
The list of existing exercises can be viewed on each tab (except "Help"),
so that the tab\'s action can be performed on an exercise on selection.
</p>
</font>
<br><br><br>
<p><font size="6"><b>Exercises</b></font></p>
<font size="4">
<p>
An exercise is characterised by three attributes: Name, Description and
Sequence.
</p><p>
The Sequence is a string conforming to the following grammar:
</p><br><p>
<tt>
&nbsp;&nbsp;S = (T|T|...|T) | SS | T<br>
&nbsp;&nbsp;T = NT | e<br>
&nbsp;&nbsp;N = A0 | A#0 | ... | C8<br>
</tt>
<br>
where "e" stands for the empty string and N can be the name of any note on a
piano, in letter notation.
</p><p>
Example sequence: C4(E4G4|G4C5)B4C5
</p><p>
Concatenation between groups represents a sequence of notes in time.<br>
Groups separated by a pipe "|" represent a choice, e.g. "(C4|E4)" is
<i>either</i> C4 <i>or</i> E4.<br>
The "|" operator has the lowest priority, so "(C4E4|G4)" should be read as
<i>either</i> C4E4 <i>or</i> G4.<br>
All space characters are ignored.
</p><p>
Exercises also have performance stats associated with them. For a more
detailed description, see the Stats section.
</p><p>
To add a new exercise, press the "+" button on the top-right. To edit an existing
exercise, select it from the "Edit" tab. For a more detailed description, see the
Edit section.
</p>
</font>
<br><br><br>
<font size="6"><b>Tabs</b></font>
<br><br>
<p><font size="5"><b>Practice</b></font></p>
<font size="4">
<p>
In the Practice mode, an exercise consists of an infinite sequence of stages.
On each stage, a selection is made, at random, from each of the pipe-separated groups
of the exercise\'s sequence (e.g. "(C4|E4)(A4|B4)" is transformed to one of: "C4A4",
"C4B4", "E4A4", "E4B4"). The resulting selection is then played, and can be replayed,
by pressing "Play Again".<br>
When the user has decided that they identified the sequence, they can check their answer
by pressing "I\'m ready". The played sequence is revelead, along with a prompt, inquiring
about the correctness of the user\'s choice (the exercise\'s stats are updated according
to the answer), then the exercise moves on to the next stage.
<\p><p>
At any time, press the "←" button to finish the exercise.
</p>
</font>
<br><br>
<p><font size="5"><b>Edit</b></font></p>
<font size="4">
<p>
In the Edit mode, you can select an exercise to modify. The editable fields are the Name,
Description and Sequence. To end the editing seesion, press the "←" button.<br>
When pressing the "←" button, if a change was made to any of the fields, a prompt will
appear, prompting the user to either save or discard the change.<br>
The edit menu also allows the option to delete an exercise, prompting for explicit
confirmation.<br>
All changes (modifications, deletions) are <b>permanent</b> and are reflected in future
sessions of the application.
</p>
</font>
<br><br>
<p><font size="5"><b>Stats</b></font></p>
<font size="4">
<p>
In the Stats mode, you can select an exercise for which to view the current statistics
in graph form.
</p><p>
The stats menu also allows the option to reset the stats, prompting for explicit confirmation.<br>
</p>
</font>
<br><br>
<font size="4">
<p>
Please contact us at <a href="mailto:contact@tonedear.com">contact@tonedear.com</a><br>
</p>
</font>
';
