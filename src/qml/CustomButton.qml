import QtQuick 2.5

import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

Button {
    id: outerBtn

    anchors.left: parent.left
    anchors.leftMargin: parent.width * 0.05
    anchors.right: parent.right
    anchors.rightMargin: parent.width * 0.05

    style: ButtonStyle {
        label: Text {
            renderType: Text.NativeRendering
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.family: "Helvetica"
            font.pointSize: 18
            color: btnColor
            text: outerBtn.text
        }
    }
}

