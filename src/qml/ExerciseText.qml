import QtQuick 2.5
import QtQuick.Layouts 1.1

Text {
    anchors {
        left: parent.left; leftMargin: parent.width * 0.05
    }

    renderType: Text.NativeRendering

    font {
        family: "Helvetica"
        pointSize: 15
        weight: Font.Normal
    }
}
