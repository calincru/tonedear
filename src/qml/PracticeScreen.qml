import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1

import ToneDear 1.0
import "PracticeScreen.js" as PracticeLogic
import "."

ColumnLayout {
    property Exercise exercise
    property int answerCnt: 0
    property int correctAnswerCnt: 0

    Component.onCompleted: {
        PracticeLogic.load_exercise();

        playBtn.clicked.connect(ToneDear.play_sequence);
        readyBtn.clicked.connect(PracticeLogic.swap_visibility);
        yesBtn.clicked.connect(PracticeLogic.reload_correct);
        noBtn.clicked.connect(PracticeLogic.reload_incorrect);
        yesBtn.clicked.connect(PracticeLogic.log_correct);
        noBtn.clicked.connect(PracticeLogic.log_incorrect);
    }

    ColumnLayout {
        Layout.alignment: Qt.AlignCenter

        Text {
            text: exercise.name

            font {
                family: "Helvetica"
                pointSize: 20
                weight: Font.DemiBold
            }
        }

        Text {
            property string accuracy:
                answerCnt == 0
                    ? "-"
                    : ((correctAnswerCnt * 100 / answerCnt).toFixed(0) + "%")

            font.pointSize: 12
            text: "Answered: " + answerCnt + "\n"
                + "Correct answers: " + correctAnswerCnt + "\n"
                + "Accuracy: " + accuracy
        }
    }

    ColumnLayout {
        id: firstPart
        visible: true
        Layout.preferredWidth: parent.width
        spacing: 40

        ExerciseText {
            text: exercise.description
            wrapMode: Text.Wrap
            Layout.maximumWidth: parent.width * 0.95
        }

        ExerciseText {
            text: exercise.seq
            wrapMode: Text.Wrap
            Layout.maximumWidth: parent.width * 0.95
        }

        RowLayout {
            Layout.alignment: Qt.AlignHCenter
            spacing: parent.width * 0.1

            Button {
                id: playBtn
                text: "Play Again"
            }

            Button {
                id: readyBtn
                text: "I'm ready"
            }
        }
    }

    ColumnLayout {
        id: secondPart
        visible: false
        Layout.preferredWidth: parent.width
        spacing: 40

        ExerciseText {
            text: "The sequence was:"
            wrapMode: Text.Wrap
        }

        ExerciseText {
            id: secretSeq
            wrapMode: Text.Wrap
        }

        ExerciseText {
            text: "Was your prediction correct?"
            wrapMode: Text.Wrap
        }

        RowLayout {
            Layout.alignment: Qt.AlignHCenter
            spacing: parent.width * 0.1

            Button {
                id: yesBtn
                text: "Yes"
            }

            Button {
                id: noBtn
                text: "No"
            }
        }
    }
}
