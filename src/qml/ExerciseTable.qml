import QtQuick 2.5

import ToneDear 1.0
import "NavigationLogic.js" as NavigationLogic

ListView {
    model: ToneDear.exercises
    delegate: WideButton {
        text: ToneDear.exercises[index].name
        onClicked: NavigationLogic.enterExercise(index, selectedTab)
    }
}
