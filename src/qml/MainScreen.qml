import QtQuick 2.5
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.4

import ToneDear 1.0

ColumnLayout {
    property string selectedTab: tabView.getTab(tabView.currentIndex).title

    Component.onCompleted: {
        selectedTab = _;
    }

    Loader {
        id: tabScreenLoader

        Layout.fillWidth:true
        Layout.fillHeight: true

        source: selectedTab == "Help" ? "HelpScreen.qml" : "ExerciseTable.qml"
    }

    TabView {
        id: tabView
        Layout.maximumHeight: 15
        Layout.alignment: Qt.AlignBottom
        Layout.fillWidth: true
        tabPosition: Qt.BottomEdge

        Tab {
            title: "Practice"
        }

        Tab {
            title: "Edit"
        }

        Tab {
            title: "Stats"
        }

        Tab {
            title: "Help"
        }
    }
}
