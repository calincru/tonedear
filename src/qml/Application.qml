import QtQuick 2.5
import QtQuick.Window 2.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1
import "NavigationLogic.js" as NavigationLogic

import "."

ApplicationWindow {
    id: main
    visible: true
    width: 500
    height: 750

    ColumnLayout {
        anchors.fill: parent

        ToolButton {
            id: navigationButton

            Layout.alignment: Qt.AlignTop | Qt.AlignRight
            visible: !(navigationStack.home && mainScreen.selectedTab == "Help")
            text: navigationStack.home ? "+" : "←"

            onClicked: {
                if (navigationStack.home)
                    navigationStack.makeNewExercise();
                else
                    navigationStack.exitExercise();
            }
        }

        StackView {
            id: navigationStack

            property bool home: navigationStack.depth == 1
            property var exitExercise: NavigationLogic.exitExercise
            property var makeNewExercise: NavigationLogic.makeNewExercise

            Layout.fillWidth: true
            Layout.fillHeight: true

            initialItem: MainScreen {
                id: mainScreen
            }
        }
    }
}
