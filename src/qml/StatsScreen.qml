import QtQuick 2.5
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2
import QtQuick.Controls 1.4

import ToneDear 1.0
import "."

ColumnLayout {
    property Exercise exercise

    spacing: 10

    MessageDialog {
        id: clearDialog
        title: "Clear exercise history"

        icon: StandardIcon.Critical
        text: "Are you sure you want to clear the history for this exercise?"
        standardButtons: StandardButton.Yes | StandardButton.No

        onYes: {
            console.log("Reseted log for exercise " + exercise.name);
            exercise.reset_log();
        }
    }

    ColumnLayout {
        Layout.alignment: Qt.AlignCenter

        Text {
            text: exercise.name

            font {
                family: "Helvetica"
                pointSize: 20
                weight: Font.DemiBold
            }
        }

        Text {
            Layout.alignment: Qt.AlignCenter
            font.pointSize: 12
            text: "Overall Progress"
        }
    }

    ColumnLayout {
        Layout.maximumHeight: parent.height / 1.5
        spacing: 30

        Rectangle {
            Layout.fillHeight: true
            Layout.fillWidth: true

            RowLayout {
                anchors.fill: parent

                Item {
                    Layout.fillHeight: true
                    Text {
                        anchors.top: parent.top
                        text: "100%"
                        font.pointSize: 15
                    }

                    Text {
                        anchors.bottom: parent.bottom
                        text: "0%"
                        font.pointSize: 15
                    }
                }

                AccuracyPlot {
                    Layout.fillHeight: true
                    Layout.fillWidth: true

                    log: exercise.log
                    filterWindow: filterWindowSlider.value
                    plotWindow: plotWindowSlider.value
                    color: "blue"
                }
            }
        }

        GridLayout {
            columns: 3

            Text {
                text: "Filter window "
                font.pointSize: 14
            }

            Slider {
                id: filterWindowSlider
                Layout.fillWidth: true
                value: 10
                minimumValue: 5
                maximumValue: 20
                stepSize: 1
            }

            Text {
                text: filterWindowSlider.value
            }

            Text {
                text: "Plot window "
                font.pointSize: 14
            }

            Slider {
                id: plotWindowSlider
                Layout.fillWidth: true
                value: 30
                minimumValue: 5
                maximumValue: 100
                stepSize: 1
            }

            Text {
                text: plotWindowSlider.value
                Layout.fillWidth: true
            }
        }

        CustomButton {
            property color btnColor: "#DC0000"

            Layout.preferredHeight: parent.height / 15
            Layout.alignment: Qt.AlignCenter

            text: "Clear history"

            onClicked: clearDialog.open()
        }
    }
}
