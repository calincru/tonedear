function enterExercise(index, mode) {
    var componentUrl;

    if (mode === "Edit")
        componentUrl = "EditScreen.qml";
    else if (mode === "Practice")
        componentUrl = "PracticeScreen.qml";
    else if (mode === "Stats")
        componentUrl = "StatsScreen.qml";
    else return;

    var component = Qt.createComponent(componentUrl);
    console.log(component.errorString())
    var screen = component.createObject(this,
        { "exercise": ToneDear.exercises[index] });

    navigationStack.push(screen);
}

function exitExercise() {
    navigationStack.pop();
}

function makeNewExercise() {
    ToneDear.add_exercise("", "", "");
    enterExercise(ToneDear.exercises.length - 1, "Edit");
}
