function load_exercise() {
    ToneDear.generate_sequence(exercise.seq);
    ToneDear.play_sequence();

    secretSeq.text = ToneDear.get_sequence();
}

function swap_visibility() {
    firstPart.visible = !firstPart.visible;
    secondPart.visible = !secondPart.visible;
}

function reload_incorrect() {
    load_exercise();
    swap_visibility();

    /* Increment in ToneDear logic the total number of answers */
    answerCnt++;
}

function reload_correct() {
    reload_incorrect();

    /* Increment in ToneDear logic the number of correct answers */
    correctAnswerCnt++;
}

function log_correct() {
    exercise.add_to_log(true);
}

function log_incorrect() {
    exercise.add_to_log(false);
}
