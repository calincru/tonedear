import QtQuick 2.0

Canvas {
    property var log
    property int filterWindow
    property int plotWindow
    property string color
    property var points

    onLogChanged: filter()
    onFilterWindowChanged: filter()
    onPlotWindowChanged: filter()

    function filter() {
        points = [];

        var endI = log.length - filterWindow + 1;
        var startI = endI - plotWindow;

        for (var i = startI; i < endI; ++i) {
            if (i < 0)
                points.push(NaN);
            else {
                var sum = 0;
                for (var j = 0; j < filterWindow; ++j)
                    sum += log[i + j];
                points.push(sum / filterWindow);
            }
        }

        canvasSizeChanged();
    }

    onCanvasSizeChanged: {
        if (available) {
            getContext('2d').reset();
            requestPaint();
        }
    }

    onPaint: {
        var ctx = getContext("2d");
        ctx.scale(width, height);
        ctx.lineWidth = 0.007;
        ctx.strokeStyle = color;

        ctx.beginPath();
        ctx.moveTo(0, 1 - points[0]);

        for (var i = 0; i < points.length; i++)
            ctx.lineTo(i / (points.length - 1), 1 - points[i]);
        ctx.stroke();
    }
}
