// This program is free software licenced under MIT Licence. You can
// find a copy of this licence in LICENCE.txt in the top directory of
// source code.
//
// Copyright (C) 2016 AproapeBine (Calin Cruceru, Mihai Dumitru, Radu Oancea)
//

#ifndef _TONEDEAR_LOGIC_HPP
#define _TONEDEAR_LOGIC_HPP 1

// Project
#include "exercise.hpp"
#include "db_manager.hpp"
#include "sequence_generator/sequence_generator.hpp"

// Qt
#include <QObject>
#include <QQmlListProperty>

namespace tonedear {

class tonedear_logic : public QObject {

    Q_OBJECT
    Q_PROPERTY(QQmlListProperty<tonedear::exercise> exercises
               READ exercises
               NOTIFY exercises_changed)

public:
    tonedear_logic(db_manager &db_mgr, QObject *parent = 0);
    ~tonedear_logic();

    // Exercise related
    QQmlListProperty<tonedear::exercise> exercises();
    Q_INVOKABLE void add_exercise(const QString &name,
                                  const QString &description,
                                  const QString &seq);
    Q_INVOKABLE void remove_exercise(exercise *ex);

    Q_INVOKABLE void play_sequence() const;
    Q_INVOKABLE QString get_sequence() const;
    Q_INVOKABLE void generate_sequence(const QString &seq_string);
    Q_INVOKABLE bool validate_sequence(const QString &seq_string);

Q_SIGNALS:
    void exercises_changed();

private:
    db_manager &m_db_mgr;
    QList<exercise *> m_exercises;
    int m_max_ex_id;
    generated_seq_ptr m_seq;

}; // class tonedear_logic

} // namespace tonedear
#endif // _TONEDEAR_LOGIC_HPP

