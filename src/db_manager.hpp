// This program is free software licenced under MIT Licence. You can
// find a copy of this licence in LICENCE.txt in the top directory of
// source code.
//
// Copyright (C) 2016 AproapeBine (Calin Cruceru, Mihai Dumitru, Radu Oancea)
//

#ifndef _DB_MANAGER_HPP
#define _DB_MANAGER_HPP 1

// Project
#include "exercise.hpp"

// Qt
#include <QString>
#include <QList>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>

namespace tonedear {

static inline void run_query(QSqlQuery &q, std::string msg) {
    if (!q.exec()) {
        qDebug() << (msg + ": ").c_str() << q.lastError();
        exit(EXIT_FAILURE);
    }
}

class db_manager {
public:
    db_manager(QString path);
    ~db_manager() = default;

    void add_exercise(const exercise &ex);
    QList<exercise *> get_exercises();

private:
    QSqlDatabase m_db;

}; // class db_manager

} // namespace tonedear
#endif // _DB_MANAGER_HPP

