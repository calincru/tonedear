// This program is free software licenced under MIT Licence. You can
// find a copy of this licence in LICENCE.txt in the top directory of
// source code.
//
// Copyright (C) 2016 AproapeBine (Calin Cruceru, Mihai Dumitru, Radu Oancea)
//

// Self
#include "db_manager.hpp"

// Qt
#include <QDebug>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlError>

// C++
#include <cstdlib>

namespace tonedear {

db_manager::db_manager(QString path)
    : m_db{QSqlDatabase::addDatabase("QSQLITE")} {
    m_db.setDatabaseName(path);

    qDebug() << "Connecting to database: " << path;
    if (!m_db.open()) {
        qDebug() << "Error: Connection with database failed";
        exit(EXIT_FAILURE);
    }

    // Create tables if needed
    QSqlQuery query{"CREATE TABLE IF NOT EXISTS Exercises"
                        "( id INTEGER PRIMARY KEY"
                        ", name TEXT"
                        ", description TEXT"
                        ", seq TEXT"
                        ", log BLOB"
                        ")",
                     m_db};
    run_query(query, "Error: Couldn't create table Exercises");
}

QList<exercise *> db_manager::get_exercises() {
    QSqlQuery query{"SELECT * FROM Exercises", m_db};

    run_query(query, "Retrieving exercises failed");

    auto id_index = query.record().indexOf("id");
    auto exercises = QList<exercise*>{};

    while (query.next()) {
        auto id = query.value(id_index).toInt();
        exercises.append(new exercise(id));
    }
    return exercises;
}

} // namespace tonedear
