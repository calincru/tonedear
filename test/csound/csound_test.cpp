#include "sequence_generator/sequence_generator.hpp"
#include "parser/parser.hpp"

#include <csound/csound.hpp>
#include <csound/csPerfThread.hpp>

#include <string>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <ios>
#include <climits>
#include <cinttypes>
#include <cstdio>

using namespace tonedear;

const std::string test = "E4 E4 F4 G4 G4 F4 E4 D4 C4 C4 D4 E4 E4 D4 D4";

static void csound_no_messages(CSOUND *, int, const char *, va_list) {}

static void print(Csound *csound)
{
    while (true) {
        auto ret = csound->PerformKsmps();
        if (ret)
            break;

        auto bufsiz = csound->GetOutputBufferSize();
        auto buffer = csound->GetSpout();

        std::vector<int16_t> samples(bufsiz);
        for (size_t i = 0; i < (size_t) bufsiz; ++i)
            samples[i] = buffer[i];
        fwrite(&samples[0], sizeof(samples[0]), samples.size(), stdout);
    }
}

int main()
{
    /* no output to stderr */
    csoundSetDefaultMessageCallback(csound_no_messages);

    //create an instance of Csound
    Csound* csound = new Csound(0);

    //set CsOptions
    //csound->SetOption((char *) "-d"); /* no display */
    //csound->SetOption((char *) "-n"); /* no output file */

    csound->SetDebug(0);

    auto seq = generate_sequence(parse_musical_seq(test));
    csound->CompileOrc(sequence_to_csound_orchestra(seq).c_str());
    csound->ReadScore(sequence_to_csound_score(seq).c_str());
    csound->Start();

    print(csound);
    delete csound;

    return 0;
}

