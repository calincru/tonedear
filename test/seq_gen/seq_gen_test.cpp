// This program is free software licenced under MIT Licence. You can
// find a copy of this licence in LICENCE.txt in the top directory of
// source code.
//
// Copyright (C) 2016 AproapeBine (Calin Cruceru, Mihai Dumitru, Radu Oancea)
//

// Project
#include "parser/parser.hpp"
#include "sequence_generator/sequence_generator.hpp"

// C++
#include <iostream>
#include <fstream>
#include <iomanip>
#include <cassert>
#include <string>

using namespace tonedear;

#define ARRAYSIZE(arr) (sizeof(arr) / sizeof(0[arr]))
const std::string valid_arr[] = {
    "C4",
    "C#4",
    "C4D5",
    "(C4|C5)",
    "(C4)",
    "(C4E4|C5E6)",
    "C4(C5|C6)",
    "C4D4(G3|A#2A3)A#1B1",
    "(C4|C#4|D4|D#4|E4)",
    "   \tC4\t\t ",
    "   (  E2 |   \t F1    \n)   ",
    " F#2  (  E2 |  \t B1 \n) B4  B4  ",
    "(C4|D4)(G4|A4)",
    "(C4|G4)(E4|B4)(E4|B4)(C4|G4)(A3|E4)(C4|G4)",
};

const std::string invalid_arr[] = {
    "J2",
    "E#4",
    "C9",
    "((A3|B3)",
    "A5 | B6",
};

#define test_note(note, st, oct) \
    {\
        assert(note->semitone == st);\
        assert(note->octave == oct);\
    }\

static void test_0() {
    auto result = parse_musical_seq(valid_arr[0]);
    assert(result);

    test_note(result->subseq->notes[0], 0, 4);
}

static void test_1()
{
    auto result = parse_musical_seq(valid_arr[1]);
    assert(result);

    test_note(result->subseq->notes[0], 1, 4);
}

static void test_2()
{
    auto result = parse_musical_seq(valid_arr[2]);
    assert(result);

    test_note(result->subseq->notes[0], 0, 4);
    test_note(result->subseq->notes[1], 2, 5);
}

static void test_3()
{
    auto result = parse_musical_seq(valid_arr[3]);
    assert(result);

    test_note(result->choice->subseqs[0]->notes[0], 0, 4);
    test_note(result->choice->subseqs[1]->notes[0], 0, 5);
}

static void test_4()
{
    auto result = parse_musical_seq(valid_arr[4]);
    assert(result);

    test_note(result->choice->subseqs[0]->notes[0], 0, 4);
}

static void test_5()
{
    auto result = parse_musical_seq(valid_arr[5]);
    assert(result);

    test_note(result->choice->subseqs[0]->notes[0], 0, 4);
    test_note(result->choice->subseqs[0]->notes[1], 4, 4);
    test_note(result->choice->subseqs[1]->notes[0], 0, 5);
    test_note(result->choice->subseqs[1]->notes[1], 4, 6);
}

static void test_6()
{
    auto result = parse_musical_seq(valid_arr[6]);
    assert(result);

    test_note(result->seq1->subseq->notes[0], 0, 4);
    test_note(result->seq2->choice->subseqs[0]->notes[0], 0, 5);
    test_note(result->seq2->choice->subseqs[1]->notes[0], 0, 6);
}

static void test_7()
{
    auto result = parse_musical_seq(valid_arr[7]);
    assert(result);

    test_note(result->seq1->subseq->notes[0], 0, 4);
    test_note(result->seq1->subseq->notes[1], 2, 4);
    test_note(result->seq2->seq1->choice->subseqs[0]->notes[0], 7, 3);
    test_note(result->seq2->seq1->choice->subseqs[1]->notes[0], 10, 2);
    test_note(result->seq2->seq1->choice->subseqs[1]->notes[1], 9, 3);
    test_note(result->seq2->seq2->subseq->notes[0], 10, 1);
    test_note(result->seq2->seq2->subseq->notes[1], 11, 1);
}

static void test_8()
{
    auto result = parse_musical_seq(valid_arr[8]);
    assert(result);

    test_note(result->choice->subseqs[0]->notes[0], 0, 4);
    test_note(result->choice->subseqs[1]->notes[0], 1, 4);
    test_note(result->choice->subseqs[2]->notes[0], 2, 4);
    test_note(result->choice->subseqs[3]->notes[0], 3, 4);
    test_note(result->choice->subseqs[4]->notes[0], 4, 4);
}

static void test_9()
{
    auto result = parse_musical_seq(valid_arr[9]);
    assert(result);

    test_note(result->subseq->notes[0], 0, 4);
}

static void test_10()
{
    auto result = parse_musical_seq(valid_arr[10]);
    assert(result);

    test_note(result->choice->subseqs[0]->notes[0], 4, 2);
    test_note(result->choice->subseqs[1]->notes[0], 5, 1);
}

static void test_11()
{
    auto result = parse_musical_seq(valid_arr[11]);
    assert(result);

    test_note(result->seq1->subseq->notes[0], 6, 2);
    test_note(result->seq2->seq1->choice->subseqs[0]->notes[0], 4, 2);
    test_note(result->seq2->seq1->choice->subseqs[1]->notes[0], 11, 1);
    test_note(result->seq2->seq2->subseq->notes[0], 11, 4);
    test_note(result->seq2->seq2->subseq->notes[1], 11, 4);
}

static void test_12()
{
    auto result = parse_musical_seq(valid_arr[12]);
    assert(result);

    test_note(result->seq1->choice->subseqs[0]->notes[0], 0, 4);
    test_note(result->seq1->choice->subseqs[1]->notes[0], 2, 4);
    test_note(result->seq2->choice->subseqs[0]->notes[0], 7, 4);
    test_note(result->seq2->choice->subseqs[1]->notes[0], 9, 4);
}

static void test_error()
{
    for (auto i = 0u; i < ARRAYSIZE(invalid_arr); ++i) {
        auto result = parse_musical_seq(invalid_arr[i]);
        assert(!result);
    }
}

static void parser_tests()
{
    test_0();
    test_1();
    test_2();
    test_3();
    test_4();
    test_5();
    test_6();
    test_7();
    test_8();
    test_9();
    test_10();
    test_11();
    test_12();
    test_error();
}

static void sequence_generator_tests()
{
    std::cout << sequence_to_string(generate_sequence(parse_musical_seq(valid_arr[3]))) << std::endl;
    std::cout << sequence_to_string(generate_sequence(parse_musical_seq(valid_arr[4]))) << std::endl;
    std::cout << sequence_to_string(generate_sequence(parse_musical_seq(valid_arr[5]))) << std::endl;
    std::cout << sequence_to_string(generate_sequence(parse_musical_seq(valid_arr[6]))) << std::endl;
    std::cout << sequence_to_string(generate_sequence(parse_musical_seq(valid_arr[7]))) << std::endl;
    std::cout << sequence_to_string(generate_sequence(parse_musical_seq(valid_arr[8]))) << std::endl;
    std::cout << sequence_to_string(generate_sequence(parse_musical_seq(valid_arr[13]))) << std::endl;
}

static void csound_program_test()
{
    auto seq = generate_sequence(parse_musical_seq(valid_arr[13]));
    std::cout << "String:\n" << sequence_to_string(seq) << std::endl;
    std::cout << "\nCsound:\n" << sequence_to_csound_score(seq) << std::endl;
}

static void create_csound_file()
{
    std::ofstream out("csound_test.csd");
    auto seq = generate_sequence(parse_musical_seq(valid_arr[13]));

    out << "<CsoundSynthesizer>\n";

    out << "<CsInstruments>\n";
    out << sequence_to_csound_orchestra(seq) << '\n';
    out << "</CsInstruments>\n";

    out << "<CsScore>\n";
    out << sequence_to_csound_score(seq) << '\n';
    out << "</CsScore>\n";

    out << "</CsoundSynthesizer>\n";
}

int main()
{
    parser_tests();
    // sequence_generator_tests();
    csound_program_test();
    create_csound_file();

    return 0;
}

