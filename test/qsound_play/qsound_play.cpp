#include <QApplication>
#include <QSound>
#include <iostream>

int main(int argc, char *argv[])
{
    QApplication app{argc, argv};
    const auto wav = "test.wav";

    QSound::play(wav);
    std::cout << "termina play" << std::endl;

    return app.exec();
}
