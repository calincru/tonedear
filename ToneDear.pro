TEMPLATE = app

# includepath
win32:LIBS += -L"$$PWD/csound/libs/win32" -lcsound64
win32:QMAKE_PRE_LINK += copy /Y "\"$$PWD/csound/libs/win32\csound64.dll\"" debug &&
win32:QMAKE_PRE_LINK += copy /Y "\"$$PWD/csound/libs/win32\libsndfile-1.dll\"" debug
android:LIBS += -L"$$PWD/csound/libs/armeabi-v7a" -lcsoundandroid -lgnustl_shared -lsndfile

linux:!android:LIBS += -L"$$PWD/csound/libs/win32" -lcsound64

deploy.depends = debug
deploy.commands = $(MKDIR) deploy &&
deploy.commands += $(COPY_FILE) "$$OUT_PWD/debug/$$TARGET" "$$OUT_PWD/deploy/$$TARGET" &&
deploy.commands += windeployqt --release "$$OUT_PWD/deploy"
QMAKE_EXTRA_TARGETS += deploy


INCLUDEPATH += "$$PWD/csound/include"
DEPENDPATH += "$$PWD/csound/include"


QT += multimedia qml quick sql
!no_desktop: QT += widgets
CONFIG += c++11

SOURCES += $$files(src/*.cpp) \
    $$files(src/parser/*.cpp) \
    $$files(src/sequence_generator/*.cpp) \
    $$files(src/utils/*.c)

HEADERS += $$files(src/*.hpp) \
    $$files(src/parser/*.hpp) \
    $$files(src/sequence_generator/*.hpp) \
    $$files(src/utils/*.h)

DISTFILES += $$files(src/qml/*.qml) $$files(src/qml/*.js) \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat
RESOURCES += src/qml/tonedear.qrc
TARGET = ToneDear

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

contains(ANDROID_TARGET_ARCH,armeabi-v7a) {
    ANDROID_EXTRA_LIBS = \
        $$PWD/csound/libs/armeabi-v7a/libcsoundandroid.so \
        $$PWD/csound/libs/armeabi-v7a/libgnustl_shared.so \
        $$PWD/csound/libs/armeabi-v7a/libsndfile.so
}

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
